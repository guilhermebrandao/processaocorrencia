package exception;

public class GeralException extends Exception {

	private static final long serialVersionUID = 6819455799555437484L;

	/**
	 * Constroe o objeto de forma padr�o.
	 */
	public GeralException() {
	}

	/**
	 * Constroe o objeto passando como parametro uma exception.
	 * 
	 * @param e
	 *            Objeto do tipo Exception.
	 */
	public GeralException(final Exception e) {
		super(e);
	}

	/**
	 * Constroe o objeto passando como parametro uma string.
	 * 
	 * @param x
	 *            String com a descri��o da exception.
	 */
	public GeralException(final String x) {
		super(x);
	}

	/**
	 * Constroe o objeto passando como parametro throwable.
	 * 
	 * @param cause
	 *            Objeto throwable.
	 */
	public GeralException(final Throwable cause) {
		super(cause);
	}
}

