package model;

public class Mensagem {
	private String descMensagem;

	/**
	 * @return the descMensagem
	 */
	public final String getDescMensagem() {
		return descMensagem;
	}

	/**
	 * @param descMensagem
	 *            the descMensagem to set
	 */
	public final void setDescMensagem(String descMensagem) {
		this.descMensagem = descMensagem;
	}
}
