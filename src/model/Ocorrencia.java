package model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

public class Ocorrencia {

	private Calendar aDtMovto;
	private String aModelo;
	private String aParcProc;
	private String aInscSeq;
	private String aOrgao;
	private String aAgencia;
	private BigDecimal aVlrCalculado;
	private BigDecimal aVlrAutenticado;
	private BigDecimal aVlrDiferenca;
	private List<Mensagem> aMensagens;
	private int atca;

	/**
	 * @return the aAgencia
	 */
	public final String getaAgencia() {
		return aAgencia;
	}

	/**
	 * @return the aDtMovto
	 */
	public final Calendar getaDtMovto() {
		return aDtMovto;
	}

	/**
	 * @return the aInscSeq
	 */
	public final String getaInscSeq() {
		return aInscSeq;
	}

	/**
	 * @return the aMensagens
	 */
	public final List<Mensagem> getaMensagens() {
		return aMensagens;
	}

	/**
	 * @return the aModelo
	 */
	public final String getaModelo() {
		return aModelo;
	}

	/**
	 * @return the aOrgao
	 */
	public final String getaOrgao() {
		return aOrgao;
	}

	/**
	 * @return the aParcProc
	 */
	public final String getaParcProc() {
		return aParcProc;
	}

	/**
	 * @return the aVlrAutenticado
	 */
	public final BigDecimal getaVlrAutenticado() {
		return aVlrAutenticado;
	}

	/**
	 * @return the aVlrCalculado
	 */
	public final BigDecimal getaVlrCalculado() {
		return aVlrCalculado;
	}

	/**
	 * @return the aVlrDiferenca
	 */
	public final BigDecimal getaVlrDiferenca() {
		return aVlrDiferenca;
	}

	/**
	 * @param pAgencia
	 *            the aAgencia to set
	 */
	public final void setaAgencia(String pAgencia) {
		this.aAgencia = pAgencia;
	}

	/**
	 * @param pDtMovto
	 *            the aDtMovto to set
	 */
	public final void setaDtMovto(Calendar pDtMovto) {
		this.aDtMovto = pDtMovto;
	}

	/**
	 * @param pInscSeq
	 *            the aInscSeq to set
	 */
	public final void setaInscSeq(String pInscSeq) {
		this.aInscSeq = pInscSeq;
	}

	/**
	 * @param pMensagens
	 *            the aMensagens to set
	 */
	public final void setaMensagens(List<Mensagem> pMensagens) {
		this.aMensagens = pMensagens;
	}

	/**
	 * @param pModelo
	 *            the aModelo to set
	 */
	public final void setaModelo(String pModelo) {
		this.aModelo = pModelo;
	}

	/**
	 * @param pOrgao
	 *            the aOrgao to set
	 */
	public final void setaOrgao(String pOrgao) {
		this.aOrgao = pOrgao;
	}

	/**
	 * @param pParcProc
	 *            the aParcProc to set
	 */
	public final void setaParcProc(String pParcProc) {
		this.aParcProc = pParcProc;
	}

	/**
	 * @param pVlrAutenticado
	 *            the aVlrAutenticado to set
	 */
	public final void setaVlrAutenticado(BigDecimal pVlrAutenticado) {
		this.aVlrAutenticado = pVlrAutenticado;
	}

	/**
	 * @param pVlrCalculado
	 *            the aVlrCalculado to set
	 */
	public final void setaVlrCalculado(BigDecimal pVlrCalculado) {
		this.aVlrCalculado = pVlrCalculado;
	}

	/**
	 * @param pVlrDiferenca
	 *            the aVlrDiferenca to set
	 */
	public final void setaVlrDiferenca(BigDecimal pVlrDiferenca) {
		this.aVlrDiferenca = pVlrDiferenca;
	}

	public final String getaMensagensToString(){
		StringBuilder sb = new StringBuilder();
		for (Mensagem mensagem : this.aMensagens) {
			sb.append(mensagem.getDescMensagem());
			sb.append(" |*| ");
		}
		
		return sb.toString();
	}

	/**
	 * @return the atca
	 */
	public final int getAtca() {
		return atca;
	}

	/**
	 * @param atca the atca to set
	 */
	public final void setAtca(int atca) {
		this.atca = atca;
	}
}
