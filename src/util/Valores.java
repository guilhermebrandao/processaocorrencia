package util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * 
 * @author guilherme.brandao
 *
 */
public abstract class Valores {

	private final static String PATTERN = "#,##0.##;-#,##0.##";

	/**
	 * Formata a string de para poder ser convertida em formato ANSI.
	 * 
	 * @param valorNaoFormatado
	 *            Formato x.xxx,xx.
	 * @return Retorna String formatada em modelo ANSI (xxxx.xx).
	 */
	public static String formataValor(final String valorNaoFormatado) {
		// Classe para
		NumberFormat format = NumberFormat.getInstance(Locale.GERMANY);

		try {
			format.parse(valorNaoFormatado);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return format.toString();
	}

	/**
	 * Cria um objeto BigDecimal a partir de uma String com o valor j�
	 * formatado.
	 * 
	 * @param valorFormatado
	 *            String "#,##0.00"
	 * @return Retorna instancia de BigDecimal com valor formatado "#,##0.00".
	 */
	public static BigDecimal criarBigDecimal(final String valorFormatado) {
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		String valor = null;
		try {
			decimalFormat.parse(valorFormatado);
			valor = decimalFormat.parse(valorFormatado).toString();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new BigDecimal(valor);
	}

	public static String formatarBigDecimal(final BigDecimal valor) {

		DecimalFormat decimalFormat = new DecimalFormat(PATTERN);

		String s = decimalFormat.format(valor);

		return s;
	}
}
