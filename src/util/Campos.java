package util;

public abstract class Campos {

	/**
	 * Adiciona o zero no inicio, caso o c�digo do banco for menor do que 6
	 * digitos, e adiciona o tra�o.
	 *
	 * @param banco
	 *            String representando c�digo do banco a ser processado.
	 * @return Retorna uma String com o c�digo do banco no formato 'xxxx-x'.
	 */
	public static String formataOrgao(final String banco) {
		StringBuilder builder = new StringBuilder(banco);

		if (banco.length() < 5) {
			builder.insert(0, '0');
			builder.insert(4, '-');
		} else {
			builder.insert(4, '-');
		}
		return builder.toString();
	}

	/**
	 * Adiciona o tra�o do d�gito no numero da Inscri��o/Sequencial.
	 *
	 * @param inscSeq
	 *            String representando a Inscri��o/Sequencial a ser processado.
	 * @return Retorna uma String com o a Inscri��o/Sequencial no formato
	 *         'xxxxxx-x'
	 */
	public static String formataInscSeq(final String inscSeq) {
		StringBuilder builder = new StringBuilder(inscSeq);

		builder.insert(builder.length() - 1, "-");

		return builder.toString();
	}
}
