package util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * @author guilherme.brandao
 *
 */
public abstract class Datas {

	/**
	 * Cria uma data.
	 *
	 * @param dia
	 *            O dia.
	 * @param mes
	 *            O m�s.
	 * @param ano
	 *            O ano.
	 * @return Uma inst�ncia da classe Date.
	 */
	public static Date criarData(final int dia, final int mes, final int ano) {
		Calendar calendario;

		calendario = Calendar.getInstance();
		calendario.set(ano, mes - 1, dia, 0, 0, 0);
		return calendario.getTime();
	}

	/**
	 * Subtrai dias da data informada.
	 * 
	 * @param data
	 *            Objeto Date a ter uma quantidade de dias subtra�dos.
	 * @param numeroDias
	 *            Inteiro representando a quantidade de dias a serem subtra�dos
	 * @return Date Retorna objeto Date com dias subtra�dos.
	 */
	public static Date subtrairDias(final Date data, final int numeroDias) {
		Calendar cal = converterDateToCalendar(data);
		cal.add(Calendar.DATE, numeroDias * -1);
		return cal.getTime();
	}

	/**
	 * Adiciona dias � data informada.
	 * 
	 * @param data
	 *            Objeto Date a ter uma quantidade de dias adicionados.
	 * @param numeroDias
	 *            Inteiro representando a quantidade de dias a serem
	 *            adicionados.
	 * @return Date Retorna objeto Date com dias adicionados.
	 */
	public static Date adicionarDias(final Date data, final int numeroDias) {
		Calendar cal = converterDateToCalendar(data);
		cal.add(Calendar.DATE, numeroDias);
		return cal.getTime();
	}

	/**
	 * Cria uma data.
	 *
	 * @param dia
	 *            O dia.
	 * @param mes
	 *            O m�s.
	 * @param ano
	 *            O ano.
	 * @param hora
	 *            A hora.
	 * @param minuto
	 *            Os minutos.
	 * @return Uma inst�ncia da classe Date.
	 */
	public static Date criarData(final int dia, final int mes, final int ano, final int hora,
			final int minuto) {
		Calendar calendario;

		calendario = Calendar.getInstance();
		calendario.set(ano, mes - 1, dia, hora, minuto, 0);
		return calendario.getTime();
	}

	/**
	 * Criar uma data.
	 *
	 * @param dataFormatada
	 *            yyyy-MM-dd.
	 * @return Retorna uma instancia da classe Date.
	 */
	public static Date criarData(final String dataFormatada) {
		Date data = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
			data = format.parse(dataFormatada);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Cria uma data do tipo java.sql.Date.
	 *
	 * @param dataUtil
	 *            Um objeto java.tratarocorrencia.util.Date.
	 * @return Uma inst�ncia da classe Date.
	 */
	public static java.sql.Date criarDataSQL(final java.util.Date dataUtil) {
		java.sql.Date retorno;
		retorno = new java.sql.Date(dataUtil.getTime());
		return retorno;
	}

	/**
	 * Cria uma data do tipo java.sql.Date.
	 * 
	 * @param calendar
	 *            Um objeto do tipo Calendar.
	 * @return Uma instancia da classe Date.
	 */
	public static java.sql.Date criarDataSQL(final Calendar calendar) {
		return criarDataSQL(calendar.getTime());
	}

	/**
	 * Cria uma data do tipo java.sql.Util.Date.
	 * 
	 * @param dataSQL
	 *            Um objeto java.tratarocorrencia.util.Date.
	 * @return Uma instancia da classe java.util.Date.
	 */
	public static Date criarDataUtil(final java.sql.Date dataSQL) {
		java.util.Date retorno;
		retorno = new java.util.Date(dataSQL.getTime());
		return retorno;
	}

	/**
	 * Cria uma hora.
	 *
	 * @param hora
	 *            A hora.
	 * @param minutos
	 *            Os minutos.
	 * @param segundos
	 *            Os segundos.
	 * @return Uma inst�ncia da classe Date.
	 */
	public static Date criarHora(final int hora, final int minutos, final int segundos) {
		Calendar calendario;

		calendario = Calendar.getInstance();
		calendario.set(Calendar.HOUR_OF_DAY, hora);
		calendario.set(Calendar.MINUTE, minutos);
		calendario.set(Calendar.SECOND, segundos);
		return calendario.getTime();
	}

	/**
	 * Converte java.util.Date em java.util.Calendar.
	 * 
	 * @param data
	 *            Objeto Date a ser convertido.
	 * @return Instancia de Calendar.
	 */
	public static Calendar converterDateToCalendar(final Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal;
	}

	/**
	 * Cria uma data do tipo Calendar.
	 * 
	 * @param dia
	 *            O dia.
	 * @param mes
	 *            O m�s.
	 * @param ano
	 *            O ano.
	 * @return Uma instancia de Calendar.
	 */
	public static Calendar criarCalendar(final int dia, final int mes, final int ano) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(criarData(dia, mes, ano));
		return cal;
	}

	/**
	 * Cria uma data do tip Calendar a partir de uma data no formato
	 * "dd/MM/yyyy".
	 * 
	 * @param dataFormatada
	 *            String com a data j� formatada.
	 * @return Uma instancia de Calendar.
	 */
	public static Calendar criarCalendar(final String dataFormatada) {

		return converterDateToCalendar(criarData(dataFormatada));
	}

	/**
	 * Converte TimeStamp numa instancia de objeto do tipo Calendar.
	 * 
	 * @param timestamp
	 *            TimeStamp a ser convertida.
	 * @return Instancia de Calendar.
	 */
	public static Calendar timeStamptoCalendar(final Timestamp timestamp) {
		SimpleDateFormat format;
		format = new SimpleDateFormat("dd/MM/yyyy HH:MM");
		format.setLenient(false);
		String stringData = format.format(timestamp);
		Calendar data = Calendar.getInstance();
		try {
			data.setTimeInMillis(format.parse(stringData).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Ob�tem TimeStamp atual.
	 * 
	 * @return Retorna objeto TimeStamp.
	 */
	public static Timestamp obterTimestampAtual() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * Obt�m o dia de um objeto do tipo Date.
	 * 
	 * @param data
	 *            Objeto Date.
	 * @return Retorna um inteiro representando o DIA da data passada.
	 */
	public static int obterDia(final Date data) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(data);
		return calendario.get(Calendar.DATE);
	}

	/**
	 * Obt�m o m�s de um objeto do tipo Date.
	 * 
	 * @param data
	 *            Objeto Date.
	 * @return Retorna um inteiro representando o M�S da data passada.
	 */
	public static int obterMes(final Date data) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(data);
		return calendario.get(Calendar.MONTH) + 1;
	}

	/**
	 * Obt�m o ano de um objeto do tipo Date.
	 * 
	 * @param data
	 *            Objeto Date.
	 * @return Retorna um inteiro representando o ANO da data passada.
	 */
	public static int obterAno(final Date data) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(data);
		return calendario.get(Calendar.YEAR);
	}

	/**
	 * Transforma um objeto Date para uma string no formato "dd/MM/yyyy".
	 *
	 * @param data
	 *            O objeto Date a ser formatado.
	 * @return O texto contendo a data no formato especificado.
	 */
	public static String formatarData(final Date data) {
		return formatarData(data, "dd/MM/yyyy");
	}

	/**
	 * Transforma um objeto Date para uma String no formato "dd/MM/yyyy -
	 * HH:mm:ss".
	 * 
	 * @param data
	 *            Objeto Date a ser formatado.
	 * @return Retorna o texto contendo a data formatado.
	 */
	public static String formatarDataCompleta(final Date data) {
		return formatarData(data, "dd/MM/yyyy - HH:mm:ss");
	}

	/**
	 * Transforma um objeto Date para uma string no formato especificado.
	 *
	 * @param data
	 *            O objeto Date a ser formatado.
	 * @param formato
	 *            O formato da data.
	 * @return O texto contendo a data no formato especificado.
	 */
	public static String formatarData(final Date data, final String formato) {
		SimpleDateFormat format;
		String dataFormatada;

		if (data != null) {
			format = new SimpleDateFormat(formato);
			dataFormatada = format.format(data);
		} else {
			dataFormatada = "";
		}
		return dataFormatada;
	}

	/**
	 * Obt�m data atual.
	 * 
	 * @return Retorna uma String no format "dd/MM/yyyy" da data atual.
	 */
	public static String obterDateAtual() {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * Transforma um objeto Calendar para uma String no formato especificado
	 * ("dd/MM/yyyy").
	 * 
	 * @param calendar
	 *            O Calendar a ser formatado.
	 * @return Retorna o texto contendo a data no formato.
	 */
	public static String calendarToString(final Calendar calendar) {
		// "dd/MM/yyyy HH:mm:ss"
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(calendar.getTime());
	}

	public static String monthName(final int month) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(2016, month, 1);

		return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
	}

}