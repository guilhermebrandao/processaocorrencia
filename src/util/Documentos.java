package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import exception.GeralException;
import model.Mensagem;
import model.Ocorrencia;

/**
 * @author guilherme.brandao
 *
 */
public abstract class Documentos {
	/**
	 * Vari�veis pra atribuir valores ao passo em que o documento � processado.
	 */
	// private static String md, parcProc, inscSeq, orgao, agencia, dataString,
	// mensagem, vlrCalc,
	// vlrAut, vlrDif;

	static JFileChooser chooser;
	static List<Ocorrencia> ocorrencias;

	/**
	 * M�todo para buscar um arquivo do tipo texto no sistema.
	 *
	 * @return Retorna uma String com o caminho do arquivo.
	 */
	public static String procurarArquivo() {
		String diretorio = null;

		// Abrir janela para escolher arquivo
		chooser = new JFileChooser();

		// Define o tipo de arquivo a ser selecionado (tipo de filtro)
		FileNameExtensionFilter txtFilter = new FileNameExtensionFilter("Documento de Texto", "txt",
				"text");

		// Define que o arquivo a ser pesquisado � do tipo Texto
		chooser.setFileFilter(txtFilter);

		int returnVal = chooser.showOpenDialog(chooser);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			diretorio = chooser.getSelectedFile().getPath();

		} else {
			chooser.setVisible(false);
		}
		return diretorio;
	}

	/**
	 * Verifica se o documento existe.
	 *
	 * @param arquivo
	 *            objeto do tipo File a ser verificado se existe
	 * @return Retorna verdadeiro se o objeto for encontrado.
	 */
	public static Boolean validarArquivo(final File arquivo) {
		// Confere se o arquivo existe
		if (arquivo.exists()) {
			JOptionPane.showMessageDialog(null, "Arquivo carregado com sucesso!");
		} else {
			JOptionPane.showMessageDialog(null, "Arquivo nao existe!", "Erro", 0);
			return false;
		}
		return null;
	}

	/**
	 * Processa o documento.
	 * 
	 * @param arquivo
	 *            Documento a ser processado.
	 * @return Lista com todas as ocorrencias existentes no documento
	 *         processado.
	 * @throws GeralException
	 *             Caso haja erro na importa��o do arquivo.
	 */
	public static List<Ocorrencia> processar(final File arquivo) throws exception.GeralException {
		Mensagem msg;
		List<Mensagem> mensagens;
		Ocorrencia ocorrencia = null;
		Scanner input;

		String md, parcProc, inscSeq, orgao, agencia, dataString = null, mensagem, vlrCalc, vlrAut,
				vlrDif;
		try {
			// Abre o arquivo atrav�s do Scanner
			input = new Scanner(arquivo.toPath(), "UTF-8");

			// Lista para guardar as ocorr�ncias depois de encontradas no
			// documento
			ocorrencias = new ArrayList<Ocorrencia>();

			// L� at� o final do arquivo
			while (input.hasNextLine()) {
				msg = new Mensagem();
				mensagens = new ArrayList<Mensagem>();
				// Define que a vari�vel token receber� sempre a pr�xima linha
				String token = input.nextLine();
				String linha = token.substring(0, token.length());
				// Procura as datas de movimento no documento
				if (token.contains("ATUALIZACAO")) {
					dataString = token.substring(93, 101);
				}

				/*
				 * Separa a linha por partes pelo espa�o em branco, se achar uma
				 * linha que tenha virgula, no caso referente a virgula do
				 * decimal (105,84)
				 */
				if (linha.length() > 103 && token.contains(",")) {
					String[] partes;

					partes = linha.split("\\s+");

					// url para teste:
					// C:\Users\guilherme.brandao\Downloads\31-10-16.txt
					if (!partes[0].matches("53|75|76|81")) {
						md = partes[0];
						inscSeq = partes[1];
						parcProc = partes[2];
						orgao = partes[3];
						agencia = partes[5];
						vlrCalc = partes[6];
						vlrAut = partes[7];
						vlrDif = partes[8];

						// Adiciona primeira linha da mensagem
						mensagem = linha.substring(token.indexOf(partes[9]));
						msg.setDescMensagem(mensagem);
						mensagens.add(msg);

						// La�o para carregar as mensagens extras
						/*
						 * Enquanto a mensagem n�o for fazia, a linha n�o
						 * estiver em branco e o scanner n�o tiver chegado ao
						 * fim do documento
						 */
						while (!msg.getDescMensagem().equals("") && !linha.trim().isEmpty()
								&& input.hasNext()) {
							msg = new Mensagem();
							mensagem = input.nextLine().trim();
							msg.setDescMensagem(mensagem);
							mensagens.add(msg);
						}

						// Percorre o arraylist de mensagens e exclui uma
						// mensagem
						// caso ela esteja em branco
						for (Mensagem mensagem1 : mensagens) {
							if (mensagem1.getDescMensagem().equals("")) {
								mensagens.remove(mensagem1);
								break;
							}
						}

						ocorrencia = new Ocorrencia();
						ocorrencia.setaDtMovto(Datas.criarCalendar(dataString));
						ocorrencia.setaModelo(md);
						ocorrencia.setaParcProc(parcProc);
						ocorrencia.setaInscSeq(Campos.formataInscSeq(inscSeq));
						ocorrencia.setaOrgao(Campos.formataOrgao(orgao));
						ocorrencia.setaAgencia(agencia);
						ocorrencia.setaVlrCalculado(Valores.criarBigDecimal(vlrCalc));
						ocorrencia.setaVlrAutenticado(Valores.criarBigDecimal(vlrAut));
						ocorrencia.setaVlrDiferenca(Valores.criarBigDecimal(vlrDif));
						ocorrencia.setaMensagens(mensagens);
						switch (md) {
						case "01":
							ocorrencia.setAtca(1);
							break;
						case "06":
							ocorrencia.setAtca(1);
							break;
						case "02":
							ocorrencia.setAtca(1);
							break;
						case "03":
							ocorrencia.setAtca(2);
							break;
						case "07":
							ocorrencia.setAtca(2);
							break;

						default:
							break;

						}
						ocorrencias.add(ocorrencia);
					}

				}
			}
			input.close();
			return ocorrencias;
		} catch (IOException e) {
			throw new GeralException("Erro ao tentar importar documento\n" + e.toString());
		}
	}

	public static String arrayToCsv() {
		StringBuilder sb = new StringBuilder();
		for (Ocorrencia ocorrencia : ocorrencias) {
			sb.append(
					Datas.calendarToString(ocorrencia.getaDtMovto()) + ";" + ocorrencia.getaModelo()
							+ ";" + ocorrencia.getaInscSeq() + ";" + ocorrencia.getaParcProc() + ";"
							+ ocorrencia.getaOrgao() + ";" + ocorrencia.getaAgencia() + ";"
							+ Valores.formatarBigDecimal(ocorrencia.getaVlrCalculado()) + ";"
							+ Valores.formatarBigDecimal(ocorrencia.getaVlrAutenticado()) + ";"
							+ Valores.formatarBigDecimal(ocorrencia.getaVlrDiferenca()) + ";"
							+ ocorrencia.getaMensagensToString() + ";" + ocorrencia.getAtca());
			sb.append(System.getProperty("line.separator"));
		}
		return sb.toString();
	}

	public static void saveCsv() {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(chooser.getSelectedFile()));
			pw.println(arrayToCsv());
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public static void saveXlsx() {
		String[] header = { "DATA MOV", "MODELO", "INSC/SEQ", "PARC/PROCES", "ORG�O", "AGENCIA",
				"VLR CALCULADO", "VLR AUTENTICADO", "DIFEREN�A", "OCORR�NCIAS", "ATCA", "CARTA",
				"TIPO", "OBS", "PARC A SER APROP.", "DATA VENCIMENTO", "MOTIVO", "DESCRI��O" };

		// New Workbook
		Workbook wb = new XSSFWorkbook();

		// Cell Style for reader row
		// CellStyle cs = wb.createCellStyle();
		// cs.setFillForegroundColor(IndexedColors.LIME.getIndex());
		// cs.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		// Font f = wb.createFont();
		// f.setBoldweight(Font.BOLDWEIGHT_BOLD);
		// f.setFontHeightInPoints((short) 12);
		// cs.setFont(f);

		// New Sheet
		Sheet sheet1 = null;
		sheet1 = wb.createSheet("myData");

		int colCount = 17;

		for (int i = 0; i <= colCount; i++) {
			sheet1.createRow(0).createCell(i).setCellValue(header[i]);
		}

		for (int i = 1; i <= ocorrencias.size(); i++) {
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaDtMovto());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaModelo());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaInscSeq());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaParcProc());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaOrgao());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getaAgencia());
			sheet1.createRow(i).createCell(0)
					.setCellValue(ocorrencias.get(i - 1).getaVlrCalculado().toString());
			sheet1.createRow(i).createCell(0)
					.setCellValue(ocorrencias.get(i - 1).getaVlrAutenticado().toString());
			sheet1.createRow(i).createCell(0)
					.setCellValue(ocorrencias.get(i - 1).getaVlrDiferenca().toString());
			sheet1.createRow(i).createCell(0)
					.setCellValue(ocorrencias.get(i - 1).getaMensagensToString());
			sheet1.createRow(i).createCell(0).setCellValue(ocorrencias.get(i - 1).getAtca());

		}

		// Open fileOutputStream

		try {
			FileOutputStream os = new FileOutputStream("teste.xlsx");
			wb.write(os);
			wb.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
