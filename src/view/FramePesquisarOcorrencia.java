package view;

import java.awt.Cursor;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

/**
 * Janela para pesquisar e importar documento.
 * 
 * @author guilherme.brandao
 *
 */
public class FramePesquisarOcorrencia extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5797933176892517687L;

	/**
	 * Componente JPanel.
	 */
	private JPanel contentPane;

	/**
	 * Componente JTextField.
	 */
	private JTextField textFieldCaminho;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            Programa.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FramePesquisarOcorrencia frame = new FramePesquisarOcorrencia();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FramePesquisarOcorrencia() {
		setTitle("Pesquisar Documento");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 591, 165);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblArquivo = new JLabel("Arquivo: ");
		lblArquivo.setBounds(10, 37, 56, 14);
		contentPane.add(lblArquivo);

		textFieldCaminho = new JTextField();
		textFieldCaminho.setBounds(76, 34, 378, 20);
		contentPane.add(textFieldCaminho);
		textFieldCaminho.setColumns(10);

		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				textFieldCaminho.setText(util.Documentos.procurarArquivo());
			}
		});
		btnPesquisar.setBounds(464, 33, 101, 23);
		contentPane.add(btnPesquisar);

		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				System.err.println("\nSistema finalizado pelo usu�rio.");
				System.exit(DISPOSE_ON_CLOSE);
			}
		});
		btnFechar.setBounds(464, 68, 101, 23);
		contentPane.add(btnFechar);

		JButton btnImportar = new JButton("Processar Documento");
		btnImportar.addActionListener(documentToCSVClick);
		btnImportar.setBounds(293, 68, 161, 23);
		contentPane.add(btnImportar);

	}

	ActionListener documentToCSVClick = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Path path = Paths.get(textFieldCaminho.getText());
			try {
				util.Documentos.processar(path.toFile());
				util.Documentos.saveCsv();
				JOptionPane.showMessageDialog(rootPane,
						"Documento de Ocorrencias processados com sucesso.");
				// util.Documentos.saveXlsx();
			} catch (exception.GeralException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	};
}
